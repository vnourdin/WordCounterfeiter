# WordCounterfeiter
A program that creates credible french spelling mistakes.
Aim to replace the mistake engine included in [Invasion](https://github.com/vnourdin/invasion) being cleaner and more optimised.  
Use [LanguageTool](http://languagetool.org) to detect the type of words and counterfeit them.

## How to use
This soft can be used directly (launching the main) to create some mistakes in a sentence or a text, but it is written to use it as a library.  
You must create Words or Texts using factories, and then call the counterfeit method on the object resulting.
