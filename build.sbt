name := "WordCounterfeiter"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies += "org.languagetool" % "language-fr" % "4.2"

// Lib for testing
libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.0.5" % "test"
