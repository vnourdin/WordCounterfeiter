package wordcounterfeiter.factories

import org.languagetool.{AnalyzedToken, AnalyzedTokenReadings}
import wordcounterfeiter.utils.MyFrenchTagger
import wordcounterfeiter.words.Genders.Gender
import wordcounterfeiter.words.Numbers.Number
import wordcounterfeiter.words._

/**
  * Factory used to create a Word object from a String containing the word
  */
object WordFactory {
  private val wordTagger: MyFrenchTagger = new MyFrenchTagger

  /**
    * Function that produce a Word from a String
    *
    * @param word : String containing the word to extract and analyse
    * @return a Word object of the correct type
    */
  def produce(word: String): Word = {
    produce(wordTagger.tagAndDisambiguate(word))
  }

  /**
    * Function that produce a Word from an AnalyzedTokenReadings
    *
    * @param token : AnalyzedTokenReadings containing the word analyzed
    * @return a Word object of the correct type
    */
  def produce(token: AnalyzedTokenReadings): Word = {
    val (lemma, partOfSpeech): (String, Array[String]) =
      extractLemmaAndPosTag(token)

    produce(token.getToken, lemma, partOfSpeech)
  }

  /**
    * Function that produce a Word from an analyzed word
    *
    * @param word         : String containing the word
    * @param lemma        : lemma of the word
    * @param partOfSpeech : pos of the word
    * @return a Word object of the correct type
    */
  def produce(word: String, lemma: String, partOfSpeech: Array[String]): Word = {
    // Create the corresponding Word object, relating to the LT pos tag, see docs/tagset.txt
    partOfSpeech(0) match {
      case "N" => Noun(word, lemma, Gender(partOfSpeech(1)), Number(partOfSpeech(2)))
      case "J" => Adjective(word, lemma, Gender(partOfSpeech(1)), Number(partOfSpeech(2)))
      case "V" =>
        // if the tag is "V avoir" or "V etre" instead of "V" we need to increment the index to retrieve every information
        val index: Int = if (partOfSpeech(1).eq("avoir") || partOfSpeech(1).eq("etre")) 2 else 1
        partOfSpeech(index) match {
          case "inf" => InfinitiveVerb(word, lemma)
          case "ppr" => PresentParticipleVerb(word, lemma)
          case "ppa" => PastParticipleVerb(word, lemma, Gender(partOfSpeech(index + 1)), Number(partOfSpeech(index + 2)))
          case _ => ConjugatedVerb(word, lemma, partOfSpeech(index), partOfSpeech(index + 1), partOfSpeech(index + 2), Number(partOfSpeech(index + 3)))
        }
      case "A" => Adverb(word, lemma)
      case "I" => Interjection(word, lemma)
      case "O" => Onomatopeia(word, lemma)
      case "K" => Cardinal(word, lemma)
      case "S" => Abbreviation(word, lemma)
      case "Z" => ProperName(word, lemma)
      case "M" => Marker(word, lemma)
      case "P" => Preposition(word, lemma)
      case "C" => Conjonction(word, lemma)
      case "D" => Determiner(word, lemma)
      case "R" => Pronoun(word, lemma)
      case _ => UnknownWord(word)
    }
  }

  /**
    * Take the first token and extract his lemma and POS tag
    *
    * @param analyzedWord : list of all tokens for a word
    * @return (lemma, split pos tag)
    */
  private def extractLemmaAndPosTag(analyzedWord: AnalyzedTokenReadings): (String, Array[String]) = {
    val analysedToken: AnalyzedToken = analyzedWord.getAnalyzedToken(0)
    if (analysedToken.hasNoTag)
      ("UNKNOWN", Array("UNKNOWN"))
    else
      (analysedToken.getLemma, analysedToken.getPOSTag.split(" "))
  }
}
