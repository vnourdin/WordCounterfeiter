package wordcounterfeiter.factories

import org.languagetool.language.French
import org.languagetool.{AnalyzedSentence, JLanguageTool}
import wordcounterfeiter.text.Text
import wordcounterfeiter.words.Word

import scala.collection.mutable.ArrayBuffer

/**
  * Factory used to create a Text object from a String containing the text
  */
object TextFactory {
  private val wordTagger: JLanguageTool = new JLanguageTool(new French())

  /**
    * Function that produce a Text from a String
    *
    * @param text : String containing the text
    * @return the text analyzed as a Text object
    */
  def produce(text: String): Text = {
    // text is analyzed as a sentence to simplify
    val analyzedSentence: AnalyzedSentence = wordTagger.getAnalyzedSentence(text)
    // this buffer contains every index of words that can be counterfeited
    val counterfeitableWords: ArrayBuffer[Int] = ArrayBuffer()

    val words: List[Word] =
      analyzedSentence.getTokens.foldLeft[List[Word]](Nil)((list, token) => {
        if (!token.isWhitespace && !token.isLinebreak && !token.isPosTagUnknown) counterfeitableWords.append(list.length)
        list ::: (WordFactory.produce(token) :: Nil)
      })

    new Text(words, counterfeitableWords)
  }
}
