package wordcounterfeiter.utils

import org.languagetool.AnalyzedTokenReadings
import org.languagetool.rules.fr.FrenchPartialPosTagFilter

/**
  * FrenchPartialPosTagFilter implementation to access to the tag method
  */
class MyFrenchTagger extends FrenchPartialPosTagFilter {
  def tagAndDisambiguate(token: String): AnalyzedTokenReadings = super.tag(token).get(0)
}