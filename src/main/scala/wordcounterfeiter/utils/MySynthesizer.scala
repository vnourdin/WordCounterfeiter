package wordcounterfeiter.utils

import org.languagetool.AnalyzedToken
import org.languagetool.synthesis.FrenchSynthesizer

object MySynthesizer {
  private val synthesizer: FrenchSynthesizer = new FrenchSynthesizer()

  def synthesize(analyzedToken: AnalyzedToken, posTag: String): String = {
    val counterfeitedWritings: Array[String] = synthesizer.synthesize(analyzedToken, posTag)
    if (counterfeitedWritings.length > 0) counterfeitedWritings(0) else analyzedToken.getToken
  }
}
