package wordcounterfeiter.words

case class Onomatopeia(correctWriting: String, lemma: String) extends Word
