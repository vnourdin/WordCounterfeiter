package wordcounterfeiter.words

import scala.util.Random

object Numbers {

  sealed trait Number {
    val char: String
  }

  object Number {
    def apply(numberString: String): Number = numberString match {
      case "s" => Singular
      case "p" => Plural
      case "sp" => SingularOrPlural
      case _ => Unknown
    }

    def produceRandomVerbNumber(): Number =
      if (Random.nextBoolean)
        Singular
      else
        Plural
  }

  case object Singular extends Number {
    val char: String = "s"
  }

  case object Plural extends Number {
    val char: String = "p"
  }

  case object SingularOrPlural extends Number {
    val char: String = "sp"
  }

  case object Unknown extends Number {
    val char: String = "unknown"
  }

}