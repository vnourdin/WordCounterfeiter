package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.AdjectiveCounterfeiter
import wordcounterfeiter.words.Genders.Gender
import wordcounterfeiter.words.Numbers.Number

case class Adjective(correctWriting: String, lemma: String, gender: Gender, number: Number) extends GenderAndNumberWord {
  override def counterfeit(): Unit = AdjectiveCounterfeiter.counterfeit(this)
}
