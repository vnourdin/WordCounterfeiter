package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.PronounCounterfeiter

case class Pronoun(correctWriting: String, lemma: String) extends Word {
  override def counterfeit(): Unit = PronounCounterfeiter.counterfeit(this)
}
