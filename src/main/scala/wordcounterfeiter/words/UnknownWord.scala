package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.InactiveCounterfeiter

case class UnknownWord(correctWriting: String) extends Word {
  override val lemma: String = correctWriting

  override def counterfeit(): Unit = InactiveCounterfeiter.counterfeit(this)
}
