package wordcounterfeiter.words

case class Marker(correctWriting: String, lemma: String) extends Word
