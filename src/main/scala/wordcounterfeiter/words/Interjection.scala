package wordcounterfeiter.words

case class Interjection(correctWriting: String, lemma: String) extends Word
