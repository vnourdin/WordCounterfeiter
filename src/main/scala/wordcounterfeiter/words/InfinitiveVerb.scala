package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.InfinitiveVerbCounterfeiter

case class InfinitiveVerb(correctWriting: String, lemma: String) extends Word {
  override def counterfeit(): Unit = InfinitiveVerbCounterfeiter.counterfeit(this)
}
