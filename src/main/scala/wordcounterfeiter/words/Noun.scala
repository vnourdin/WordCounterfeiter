package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.NounCounterfeiter
import wordcounterfeiter.words.Genders.Gender
import wordcounterfeiter.words.Numbers.Number

case class Noun(correctWriting: String, lemma: String, gender: Gender, number: Number) extends GenderAndNumberWord {
  override def counterfeit(): Unit = NounCounterfeiter.counterfeit(this)
}
