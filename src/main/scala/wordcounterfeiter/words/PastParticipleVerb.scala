package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.PastParticipleVerbCounterfeiter
import wordcounterfeiter.words.Genders.Gender
import wordcounterfeiter.words.Numbers.Number

case class PastParticipleVerb(correctWriting: String, lemma: String, gender: Gender, number: Number) extends Word {
  override def counterfeit(): Unit = PastParticipleVerbCounterfeiter.counterfeit(this)
}
