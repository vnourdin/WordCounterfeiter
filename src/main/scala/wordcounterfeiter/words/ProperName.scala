package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.InactiveCounterfeiter

case class ProperName(correctWriting: String, lemma: String) extends Word {
  override def counterfeit(): Unit = InactiveCounterfeiter.counterfeit(this)
}
