package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.DeterminerCounterfeiter

case class Determiner(correctWriting: String, lemma: String) extends Word {
  override def counterfeit(): Unit = DeterminerCounterfeiter.counterfeit(this)
}
