package wordcounterfeiter.words

object Genders {

  sealed trait Gender {
    val char: String
  }

  object Gender {
    def apply(genderChar: String): Gender = genderChar match {
      case "m" => Masculine
      case "f" => Feminine
      case "e" => Epicene
      case _ => Unknown
    }
  }

  case object Masculine extends Gender {
    val char = "m"
  }

  case object Feminine extends Gender {
    val char = "f"
  }

  case object Epicene extends Gender {
    val char = "e"
  }

  case object Unknown extends Gender {
    val char = "unknown"
  }

}