package wordcounterfeiter.words

case class Preposition(correctWriting: String, lemma: String) extends Word
