package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.PresentParticipleVerbCounterfeiter

case class PresentParticipleVerb(correctWriting: String, lemma: String) extends Word {
  override def counterfeit(): Unit = PresentParticipleVerbCounterfeiter.counterfeit(this)
}
