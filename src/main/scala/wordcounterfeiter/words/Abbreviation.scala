package wordcounterfeiter.words

case class Abbreviation(correctWriting: String, lemma: String) extends Word
