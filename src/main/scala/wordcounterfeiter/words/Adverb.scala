package wordcounterfeiter.words

case class Adverb(correctWriting: String, lemma: String) extends Word
