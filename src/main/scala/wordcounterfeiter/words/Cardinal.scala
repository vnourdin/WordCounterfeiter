package wordcounterfeiter.words

case class Cardinal(correctWriting: String, lemma: String) extends Word
