package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.ConjugatedVerbCounterfeiter
import wordcounterfeiter.words.Numbers.Number

case class ConjugatedVerb(correctWriting: String, lemma: String, mood: String, tense: String, person: String, number: Number) extends Word {
  override def counterfeit(): Unit = ConjugatedVerbCounterfeiter.counterfeit(this)
}
