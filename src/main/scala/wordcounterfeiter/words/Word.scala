package wordcounterfeiter.words

import wordcounterfeiter.counterfeiters.GenericCounterfeiter
import wordcounterfeiter.words.Genders.Gender
import wordcounterfeiter.words.Numbers.Number

/**
  * Class representing a word
  */
abstract class Word() {
  val correctWriting: String
  val lemma: String
  var currentWriting: String = correctWriting

  def isCounterfeited: Boolean = this.currentWriting != this.correctWriting

  def counterfeit(): Unit = GenericCounterfeiter.counterfeit(this)

  override def toString: String = this.getClass.getSimpleName + " " + correctWriting + " -> " + currentWriting
}

abstract class GenderAndNumberWord extends Word {
  val gender: Gender
  val number: Number
}
