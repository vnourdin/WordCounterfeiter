package wordcounterfeiter

import wordcounterfeiter.factories.TextFactory
import wordcounterfeiter.text.Text

object Main extends App {
  def aux(): Unit = {
    print("Entrez un texte: ")
    val input: String = scala.io.StdIn.readLine()
    if (input != "exit") {
      val text: Text = TextFactory.produce(input)
      text.counterfeit(5)
      println(text)
      aux()
    }
  }

  println("Ce programme vous permets de créer des fautes dans un texte.")
  aux()
}
