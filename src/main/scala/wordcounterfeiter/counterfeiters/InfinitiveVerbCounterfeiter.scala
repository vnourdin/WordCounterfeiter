package wordcounterfeiter.counterfeiters

import org.languagetool.AnalyzedToken
import wordcounterfeiter.utils.MySynthesizer
import wordcounterfeiter.words.InfinitiveVerb

object InfinitiveVerbCounterfeiter extends WordCounterfeiter[InfinitiveVerb] {
  /**
    * A counterfeiter that change infinitive verbs to past participle m s form if they ends with "er"
    */
  private val counterfeitToPpa: SimpleCounterfeiter[InfinitiveVerb] = new SimpleCounterfeiter[InfinitiveVerb] {
    override def isApplicable(word: InfinitiveVerb): Boolean = word.correctWriting.endsWith("er")

    override def counterfeitImpl(word: InfinitiveVerb): String = {
      MySynthesizer.synthesize(new AnalyzedToken(word.correctWriting, "V inf", word.lemma), "V ppa m s")
    }
  }

  /**
    * Add an "e" or "re" at the end of verbs that ends with "ir" because it doesn't change the pronunciation
    */
  private val addE: SimpleCounterfeiter[InfinitiveVerb] = new SimpleCounterfeiter[InfinitiveVerb] {
    override def isApplicable(word: InfinitiveVerb): Boolean = word.correctWriting.endsWith("ir")

    override def counterfeitImpl(word: InfinitiveVerb): String = word.correctWriting + GenericCounterfeiters.randomlyChoose("e", "re")
  }

  override protected val methods: Seq[SimpleCounterfeiter[_ >: InfinitiveVerb]] =
    GenericCounterfeiter.methods ++ Seq(
      counterfeitToPpa,
      addE
    )
}
