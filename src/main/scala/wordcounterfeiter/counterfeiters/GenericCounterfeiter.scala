package wordcounterfeiter.counterfeiters

import wordcounterfeiter.words.Word

object GenericCounterfeiter extends WordCounterfeiter[Word] {
  override val methods: Seq[SimpleCounterfeiter[Word]] = Seq(
    GenericCounterfeiters.doubleConsonant,
    GenericCounterfeiters.invertHomonymsAi,
    GenericCounterfeiters.invertHomonymsO,
    GenericCounterfeiters.invertHomonymsSc,
    GenericCounterfeiters.rmLastLetter,
    GenericCounterfeiters.rmFistLetter,
    GenericCounterfeiters.splitDoubleLetters
  )
}
