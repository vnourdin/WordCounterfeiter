package wordcounterfeiter.counterfeiters

import org.languagetool.AnalyzedToken
import wordcounterfeiter.utils.MySynthesizer
import wordcounterfeiter.words.GenderAndNumberWord
import wordcounterfeiter.words.Genders.Gender
import wordcounterfeiter.words.Numbers.Number

import scala.util.Random

abstract class GenderAndNumberCounterfeiters[W <: GenderAndNumberWord] extends WordCounterfeiter[W] {
  protected def numberCounterfeiter(typeLetter: Char, newNumber: Number): SimpleCounterfeiter[W] = new SimpleCounterfeiter[W] {
    override def isApplicable(word: W): Boolean = word.number != newNumber

    override def counterfeitImpl(word: W): String =
      counterfeitGenderAndNumber(word, typeLetter, word.gender, newNumber)
  }

  protected def genderCounterfeiter(typeLetter: Char, newGender: Gender): SimpleCounterfeiter[W] = new SimpleCounterfeiter[W] {
    /**
      * Adding more random because gender mistake isn't really common
      */
    override def isApplicable(word: W): Boolean = (word.gender != newGender) && Random.nextBoolean()

    override def counterfeitImpl(word: W): String =
      counterfeitGenderAndNumber(word, typeLetter, newGender, word.number)
  }

  private def counterfeitGenderAndNumber(word: W, typeLetter: Char, newGender: Gender, newNumber: Number): String = {
    val posTag: String = typeLetter + " " + word.gender + " " + word.number
    val newPosTag: String = typeLetter + " " + newGender + " " + newNumber
    MySynthesizer.synthesize(new AnalyzedToken(word.correctWriting, posTag, word.lemma), newPosTag)
  }
}
