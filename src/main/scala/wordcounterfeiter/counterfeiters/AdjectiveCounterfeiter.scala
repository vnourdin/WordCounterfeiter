package wordcounterfeiter.counterfeiters

import wordcounterfeiter.words.Adjective
import wordcounterfeiter.words.Genders._
import wordcounterfeiter.words.Numbers._

object AdjectiveCounterfeiter extends GenderAndNumberCounterfeiters[Adjective] {
  override protected val methods: Seq[SimpleCounterfeiter[_ >: Adjective]] =
    GenericCounterfeiter.methods ++ Seq(
      numberCounterfeiter('J', Singular),
      numberCounterfeiter('J', Plural),
      genderCounterfeiter('J', Masculine),
      genderCounterfeiter('J', Feminine)
    )
}
