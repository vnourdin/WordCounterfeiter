package wordcounterfeiter.counterfeiters

import org.languagetool.AnalyzedToken
import wordcounterfeiter.utils.MySynthesizer
import wordcounterfeiter.words.{ConjugatedVerb, Numbers}

import scala.util.Random

object ConjugatedVerbCounterfeiter extends WordCounterfeiter[ConjugatedVerb] {
  /**
    * Counterfeit the conjugation of a verb
    *
    */
  private val counterfeitConjugation: SimpleCounterfeiter[ConjugatedVerb] = new SimpleCounterfeiter[ConjugatedVerb] {
    override def isApplicable(word: ConjugatedVerb): Boolean = true

    override def counterfeitImpl(word: ConjugatedVerb): String = {
      val initialPosTag: String = "V " + word.mood + " " + word.tense + " " + word.person + " " + word.number
      val initialToken: AnalyzedToken = new AnalyzedToken(word.correctWriting, initialPosTag, word.lemma)

      val newPosTag: String = counterfeitPosTag(initialPosTag)

      MySynthesizer.synthesize(initialToken, newPosTag)
    }
  }

  /**
    * @return a random mood
    */
  protected def generateMood(): String = Random.nextInt(4) match {
    case 0 => "ind"
    case 1 => "con"
    case 2 => "sub"
    case 3 => "imp"
  }

  /**
    * @return a random tense
    */
  protected def generateTense(): String = Random.nextInt(4) match {
    case 0 => "pres"
    case 1 => "psim"
    case 2 => "impa"
    case 3 => "futu"
  }

  /**
    * @return a random person
    */
  protected def generatePerson(): String = Random.nextInt(3) match {
    case 0 => "1"
    case 1 => "2"
    case 2 => "3"
  }

  /**
    * Counterfeit the pos tag relating to conjugation
    * Tail recursion call while the new pos tag is the same as the initial
    *
    * @param initialPosTag : correct pos tag of the word
    * @return the new pos tag
    */
  @scala.annotation.tailrec
  private def counterfeitPosTag(initialPosTag: String): String = {
    val newPosTag: String = generateMood() match {
      case "ind" =>
        "V ind " + generateTense + " " + generatePerson + " " + Numbers.Number.produceRandomVerbNumber()
      case "con" =>
        "V con pres " + generatePerson + " " + Numbers.Number.produceRandomVerbNumber()
      case "sub" =>
        val newTense: String = generateTense() match {
          case "impa" => "impa ";
          case _ => "pres "
        }
        "V sub " + newTense + generatePerson + " " + Numbers.Number.produceRandomVerbNumber()
      case "imp" =>
        val newPersAndNumber: String = generatePerson() match {
          case "1" => "1 p"
          case _ => "2 " + Numbers.Number.produceRandomVerbNumber()
        }
        "V imp pres " + newPersAndNumber
    }

    if (newPosTag == initialPosTag) counterfeitPosTag(initialPosTag) else newPosTag
  }

  override protected val methods: Seq[SimpleCounterfeiter[_ >: ConjugatedVerb]] =
    GenericCounterfeiter.methods ++ Seq(
      counterfeitConjugation
    )
}
