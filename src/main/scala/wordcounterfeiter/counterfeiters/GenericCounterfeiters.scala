package wordcounterfeiter.counterfeiters

import wordcounterfeiter.words.Word

import scala.util.Random
import scala.util.matching.Regex
import scala.util.matching.Regex.Match

object GenericCounterfeiters {
  /**
    * Double a consonant that is surrounded by vowels
    */
  val doubleConsonant: SimpleCounterfeiter[Word] = new SimpleCounterfeiter[Word] {
    override def isApplicable(word: Word): Boolean = true

    private val vowel = "[aeiouy]"
    private val consonant = "[cflmnprst]"

    override def counterfeitImpl(word: Word): String = {
      val pattern: Regex = new Regex(s"($vowel)($consonant)($vowel)")
      val mapper: Match => Option[String] = (m: Match) =>
        Some(randomlyChoose(m.group(1) + m.group(2) + m.group(2) + m.group(3), m.matched))

      pattern.replaceSomeIn(word.correctWriting, mapper)
    }
  }

  /**
    * Exchange homonyms: "ai" and "è"
    */
  val invertHomonymsAi: SimpleCounterfeiter[Word] = new SimpleCounterfeiter[Word] {
    override def isApplicable(word: Word): Boolean = true

    override def counterfeitImpl(word: Word): String = {
      val pattern: Regex = new Regex("(ai(?!e))|è")
      val mapper: Match => Option[String] = (_: Match) => {
        Some(randomlyChoose("ai", "è"))
      }

      pattern.replaceSomeIn(word.correctWriting, mapper)
    }
  }

  /**
    * Exchange homonyms: "eau", "au" and "o"
    */
  val invertHomonymsO: SimpleCounterfeiter[Word] = new SimpleCounterfeiter[Word] {
    override def isApplicable(word: Word): Boolean = true

    override def counterfeitImpl(word: Word): String = {
      // Match "eau", "au" and "o" followed by anything but "u" and "n" because they change the sound
      val pattern: Regex = new Regex("eau|au|o(?![unm])")
      val mapper: Match => Option[String] = (m: Match) => {
        val counterfeited: String = m.matched match {
          case "eau" => "au"
          case "au" => randomlyChoose("eau", "o")
          case _ => "au"
        }
        Some(randomlyChoose(counterfeited, m.matched))
      }

      pattern.replaceSomeIn(word.correctWriting, mapper)
    }
  }

  /**
    * Exchange homonyms: "ss", "ç", "sc" and "c"
    */
  val invertHomonymsSc: SimpleCounterfeiter[Word] = new SimpleCounterfeiter[Word] {
    override def isApplicable(word: Word): Boolean = true

    override def counterfeitImpl(word: Word): String = {
      // "sc" followed by anything else that "e" or "i" doesn't sound the same, nor c followed by "a", "u", "o" or "h"
      val pattern: Regex = new Regex("ss|ç|(sc|c)(?=[ei])")
      val mapper: Match => Option[String] = (m: Match) => {
        val counterfeited: String = m.matched match {
          case "ss" => randomlyChoose("ç", "c")
          case "ç" => randomlyChoose("ss", "c")
          case "sc" => randomlyChoose("ss", "c")
          case "c" => randomlyChoose("ç", "ss")
        }
        Some(randomlyChoose(counterfeited, m.matched))
      }

      pattern.replaceSomeIn(word.correctWriting, mapper)
    }
  }

  /**
    * Exchange homonyms: "g" and "j"
    */
  val invertHomonymsG: SimpleCounterfeiter[Word] = new SimpleCounterfeiter[Word] {
    override def isApplicable(word: Word): Boolean = true

    override def counterfeitImpl(word: Word): String = {
      // "g" followed by anything else that "e" or "i" doesn't sound like "j"
      val pattern: Regex = new Regex("g(?=[ei])")
      val mapper: Match => Option[String] = (m: Match) => {
        val counterfeited: String = m.matched match {
          case "g" => "j"
        }
        Some(randomlyChoose(counterfeited, m.matched))
      }

      pattern.replaceSomeIn(word.correctWriting, mapper)
    }
  }

  /**
    * Remove the last letter of a word if this letter is "s", "d" or "x"
    */
  val rmLastLetter: SimpleCounterfeiter[Word] = new SimpleCounterfeiter[Word] {
    override def isApplicable(word: Word): Boolean = {
      val pattern: Regex = new Regex("[sdx]$")
      word.correctWriting.length > 1 && pattern.findAllIn(word.correctWriting).hasNext
    }

    override def counterfeitImpl(word: Word): String = word.correctWriting.substring(0, word.correctWriting.length - 1)
  }

  /**
    * Remove the fist letter of a word if this letter is "h"
    */
  val rmFistLetter: SimpleCounterfeiter[Word] = new SimpleCounterfeiter[Word] {
    override def isApplicable(word: Word): Boolean = {
      val pattern: Regex = new Regex("^h")
      word.correctWriting.length > 3 && pattern.findAllIn(word.correctWriting).hasNext
    }

    override def counterfeitImpl(word: Word): String = word.correctWriting.substring(1)
  }

  /**
    * Split or not, randomly, double letters in a word
    */
  val splitDoubleLetters: SimpleCounterfeiter[Word] = new SimpleCounterfeiter[Word] {
    override def isApplicable(word: Word): Boolean = true

    override def counterfeitImpl(word: Word): String = {
      val pattern: Regex = new Regex("([a-zA-Z])\\1+")
      val mapper: Match => Option[String] = (m: Match) =>
        Some(randomlyChoose(m.group(1), m.matched))

      pattern.replaceSomeIn(word.correctWriting, mapper)
    }
  }

  /**
    * Method that randomly choose one of the string params
    *
    * @param first  : first possible choice
    * @param second : second possible choice
    * @return first or second
    */
  def randomlyChoose(first: String, second: String): String = if (Random.nextBoolean()) first else second
}
