package wordcounterfeiter
package counterfeiters

abstract class Counterfeiter[-W <: words.Word] {

  /**
    * Counterfeit the currentWriting of a word
    *
    * @param word : word to counterfeit
    */
  def counterfeit(word: W): Unit
}
