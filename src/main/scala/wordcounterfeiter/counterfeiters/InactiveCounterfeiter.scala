package wordcounterfeiter.counterfeiters

import wordcounterfeiter.words.Word

object InactiveCounterfeiter extends WordCounterfeiter[Word] {
  /**
    * Seq of every counterfeiting methods that can be applied to this Word
    */
  override protected val methods: Seq[Counterfeiter[_ >: Word]] = Seq()

  override def counterfeit(word: Word): Unit = None
}
