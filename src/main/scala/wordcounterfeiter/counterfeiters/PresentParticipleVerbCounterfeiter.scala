package wordcounterfeiter.counterfeiters

import org.languagetool.AnalyzedToken
import wordcounterfeiter.utils
import wordcounterfeiter.words.PresentParticipleVerb

object PresentParticipleVerbCounterfeiter extends WordCounterfeiter[PresentParticipleVerb] {
  private val counterfeitToPastParticiple: SimpleCounterfeiter[PresentParticipleVerb] = new SimpleCounterfeiter[PresentParticipleVerb] {
    override def isApplicable(word: PresentParticipleVerb): Boolean = true

    override def counterfeitImpl(word: PresentParticipleVerb): String =
      utils.MySynthesizer.synthesize(new AnalyzedToken(word.correctWriting, "V ppr", word.lemma), "V ppa m s")
  }

  override protected val methods: Seq[SimpleCounterfeiter[_ >: PresentParticipleVerb]] =
    GenericCounterfeiter.methods ++ Seq(
      counterfeitToPastParticiple
    )
}
