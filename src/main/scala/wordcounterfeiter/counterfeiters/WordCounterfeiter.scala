package wordcounterfeiter.counterfeiters

import wordcounterfeiter.words.Word

abstract class WordCounterfeiter[W <: Word] {
  /**
    * Seq of every counterfeiting methods that can be applied to this Word
    */
  protected val methods: Seq[Counterfeiter[_ >: W <: Word]]

  def counterfeit(word: W): Unit = new RandomCompoundCounterfeiter[W](this.methods).counterfeit(word)
}
