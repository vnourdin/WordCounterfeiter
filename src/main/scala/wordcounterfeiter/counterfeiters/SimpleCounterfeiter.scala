package wordcounterfeiter.counterfeiters

import wordcounterfeiter.words.Word

trait SimpleCounterfeiter[W <: Word] extends Counterfeiter[W] {
  override def counterfeit(word: W): Unit = {
    if (isApplicable(word)) {
      val counterfeited: String = counterfeitImpl(word)
      word.currentWriting = if (word.correctWriting.charAt(0).isUpper) {
        val first: Char = counterfeited.charAt(0)
        counterfeited.replaceFirst(first.toString, first.toUpper.toString)
      }
      else
        counterfeited
    }
  }

  def isApplicable(word: W): Boolean

  def counterfeitImpl(word: W): String
}
