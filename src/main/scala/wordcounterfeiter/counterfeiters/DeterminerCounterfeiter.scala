package wordcounterfeiter.counterfeiters

import wordcounterfeiter.words.Determiner

object DeterminerCounterfeiter extends WordCounterfeiter[Determiner] {
  private val counterfeitHomonyms: SimpleCounterfeiter[Determiner] = new SimpleCounterfeiter[Determiner] {
    override def isApplicable(word: Determiner): Boolean = true

    override def counterfeitImpl(word: Determiner): String =
      word.correctWriting match {
        case "ce" => "se"
        case "ces" => "ses"
        case "ses" => "ces"
        case _ => word.correctWriting
      }
  }

  override protected val methods: Seq[SimpleCounterfeiter[_ >: Determiner]] =
    GenericCounterfeiter.methods ++ Seq(
      counterfeitHomonyms
    )
}
