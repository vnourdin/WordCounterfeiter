package wordcounterfeiter.counterfeiters

import org.languagetool.AnalyzedToken
import wordcounterfeiter.utils
import wordcounterfeiter.words.PastParticipleVerb

object PastParticipleVerbCounterfeiter extends WordCounterfeiter[PastParticipleVerb] {
  private val counterfeitToPresentParticiple: SimpleCounterfeiter[PastParticipleVerb] = new SimpleCounterfeiter[PastParticipleVerb] {
    override def isApplicable(word: PastParticipleVerb): Boolean = true

    override def counterfeitImpl(word: PastParticipleVerb): String =
      utils.MySynthesizer.synthesize(new AnalyzedToken(word.correctWriting, "V ppa " + word.gender + " " + word.number, word.lemma), "V ppr")
  }

  override protected val methods: Seq[SimpleCounterfeiter[_ >: PastParticipleVerb]] =
    GenericCounterfeiter.methods ++ Seq(
      counterfeitToPresentParticiple
    )
}
