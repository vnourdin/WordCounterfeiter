package wordcounterfeiter.counterfeiters

import wordcounterfeiter.words.Genders._
import wordcounterfeiter.words.Noun
import wordcounterfeiter.words.Numbers._

object NounCounterfeiter extends GenderAndNumberCounterfeiters[Noun] {
  override protected val methods: Seq[SimpleCounterfeiter[_ >: Noun]] =
    GenericCounterfeiter.methods ++ Seq(
      numberCounterfeiter('N', Singular),
      numberCounterfeiter('N', Plural),
      genderCounterfeiter('N', Masculine),
      genderCounterfeiter('N', Feminine)
    )
}
