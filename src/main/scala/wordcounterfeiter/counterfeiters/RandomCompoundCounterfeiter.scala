package wordcounterfeiter.counterfeiters

import wordcounterfeiter.words.Word

import scala.util.Random

class RandomCompoundCounterfeiter[W <: Word](val methods: Seq[Counterfeiter[W]]) extends Counterfeiter[W] {

  override def counterfeit(word: W): Unit = {
    if (!word.isCounterfeited)
      Random.shuffle(methods)
        .takeWhile(c => {
          c.counterfeit(word)
          !word.isCounterfeited
        })
  }
}
