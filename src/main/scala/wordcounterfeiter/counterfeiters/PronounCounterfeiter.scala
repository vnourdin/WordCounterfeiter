package wordcounterfeiter.counterfeiters

import wordcounterfeiter.words.Pronoun

object PronounCounterfeiter extends WordCounterfeiter[Pronoun] {
  private val counterfeitHomonyms: SimpleCounterfeiter[Pronoun] = new SimpleCounterfeiter[Pronoun] {
    override def isApplicable(word: Pronoun): Boolean = true

    override def counterfeitImpl(word: Pronoun): String =
      word.correctWriting match {
        case "se" => "ce"
        case "ce" => "se"
        case _ => word.correctWriting
      }
  }

  override protected val methods: Seq[SimpleCounterfeiter[_ >: Pronoun]] =
    GenericCounterfeiter.methods ++ Seq(
      counterfeitHomonyms
    )
}
