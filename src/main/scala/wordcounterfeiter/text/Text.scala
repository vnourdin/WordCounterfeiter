package wordcounterfeiter.text

import wordcounterfeiter.words.Word

import scala.util.Random

/**
  * Class representing a text
  *
  * @param words                : Word objects composing the text
  * @param counterfeitableWords : indexes of word that can be counterfeited (not whitespace, linebreak or unknown tag)
  */
class Text(words: List[Word], counterfeitableWords: Seq[Int]) {

  val correctWriting: String = words.foldLeft("")((text, word) => text + word.correctWriting)

  /**
    * Method that try to counterfeit nbMistake words
    *
    * @param nbMistake : number of word to counterfeit
    */
  def counterfeit(nbMistake: Int): Unit = {
    val counterfeitIndexes: Seq[Int] = Random.shuffle(counterfeitableWords).take(nbMistake)
    counterfeitIndexes.foreach(index => words(index).counterfeit())
  }

  override def toString: String = correctWriting + "\n -> \n" + currentWriting

  def currentWriting(): String = words.foldLeft("")((text, word) => text + word.currentWriting)
}
