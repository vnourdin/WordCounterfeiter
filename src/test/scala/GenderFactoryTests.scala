import wordcounterfeiter.words.Genders._
import org.scalatest.FunSuite

class GenderFactoryTests extends FunSuite {
  test("Feminine") {
    assert(Gender("f") == Feminine)
  }
  test("Masculine") {
    assert(Gender("m") == Masculine)
  }
  test("Epicene") {
    assert(Gender("e") == Epicene)
  }
  test("Unknown") {
    assert(Gender("h") == Unknown)
  }
}
