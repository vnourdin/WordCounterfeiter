import wordcounterfeiter.words.Genders.{Feminine, Masculine}
import wordcounterfeiter.words.Numbers.{Plural, Singular, SingularOrPlural}
import wordcounterfeiter.words._
import org.scalatest.FunSuite
import wordcounterfeiter.factories.WordFactory

class WordFactoryTests extends FunSuite {
  test("Noun") {
    val expected: Noun = Noun("Maisons", "maison", Feminine, Plural)
    assert(WordFactory.produce("Maisons") == expected)
    assert(WordFactory.produce("Maisons", "maison", Array("N", "f", "p")) == expected)
  }
  test("Adjective") {
    val expected: Adjective = Adjective("Gros", "gros", Masculine, SingularOrPlural)
    assert(WordFactory.produce("Gros") == expected)
    assert(WordFactory.produce("Gros", "gros", Array("J", "m", "sp")) == expected)
  }
  test("InfinitiveVerb") {
    val expected: InfinitiveVerb = InfinitiveVerb("croire", "croire")
    assert(WordFactory.produce("croire") == expected)
    assert(WordFactory.produce("croire", "croire", Array("V", "inf")) == expected)
  }
  test("PresentParticipleVerb") {
    val expected: PresentParticipleVerb = PresentParticipleVerb("nageant", "nager")
    assert(WordFactory.produce("nageant") == expected)
    assert(WordFactory.produce("nageant", "nager", Array("V", "ppr")) == expected)
  }
  test("PastParticipleVerb") {
    val expected: PastParticipleVerb = PastParticipleVerb("couru", "courir", Masculine, Singular)
    // Without context, past participles are detected as noun or adjective as it's the same writing
    //assert(WordFactory.produce("couru") == PastParticipleVerb("couru", "courir", "m", "s"))
    assert(WordFactory.produce("couru", "courir", Array("V", "ppa", "m", "s")) == expected)
  }
  test("ConjugatedVerb") {
    val expected: ConjugatedVerb = ConjugatedVerb("aime", "aimer", "imp", "pres", "2", Singular)
    assert(WordFactory.produce("aime") == expected)
    assert(WordFactory.produce("aime", "aimer", Array("V", "imp", "pres", "2", "s")) == expected)
  }
  test("Adverb") {
    val expected: Adverb = Adverb("vraiment", "vraiment")
    assert(WordFactory.produce("vraiment") == expected)
    assert(WordFactory.produce("vraiment", "vraiment", Array("A")) == expected)
  }
  test("Interjection") {
    // We'll not falsify this
  }
  test("Onomatopeia") {
    // We'll not falsify this
  }
  test("Cardinal") {
    // We'll not falsify this
  }
  test("Abbreviation") {
    // We'll not falsify this
  }
  test("ProperName") {
    // We'll not falsify this
  }
  test("Marker") {
    // We'll not falsify this
  }
  test("Preposition") {
    val expected: Preposition = Preposition("de", "de")
    assert(WordFactory.produce("de") == expected)
    assert(WordFactory.produce("de", "de", Array("P")) == expected)
  }
  test("Conjonction") {
    val expected: Conjonction = Conjonction("car", "car")
    assert(WordFactory.produce("car") == expected)
    assert(WordFactory.produce("car", "car", Array("C")) == expected)
  }
  test("Determiner") {
    val expected: Determiner = Determiner("le", "le")
    assert(WordFactory.produce("le") == expected)
    assert(WordFactory.produce("le", "le", Array("D")) == expected)
  }
  test("Pronoun") {
    val expected: Pronoun = Pronoun("je", "je")
    assert(WordFactory.produce("je") == expected)
    assert(WordFactory.produce("je", "je", Array("R")) == expected)
  }
  test("UnknownWord") {
    // We'll not falsify this
  }
}
