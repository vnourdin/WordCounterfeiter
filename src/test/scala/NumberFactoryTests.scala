import org.scalatest.FunSuite
import wordcounterfeiter.words.Numbers._

class NumberFactoryTests extends FunSuite {
  test("Singular") {
    assert(Number("s") == Singular)
  }
  test("Plural") {
    assert(Number("p") == Plural)
  }
  test("SingularOrPlural") {
    assert(Number("sp") == SingularOrPlural)
  }
  test("Unknown") {
    assert(Number("h") == Unknown)
  }
}
