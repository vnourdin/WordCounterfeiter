import org.scalatest.FunSuite
import wordcounterfeiter.words.Genders.{Feminine, Masculine}
import wordcounterfeiter.words.Numbers.{Plural, Singular}
import wordcounterfeiter.words._

class CounterfeitersTests extends FunSuite {
  test("InfinitiveVerb") {
    val infinitiveVerb: InfinitiveVerb = InfinitiveVerb("croire", "croire")
    val counter: Int = testWord(infinitiveVerb)
    assert(counter > 0)
  }
  test("ConjugatedVerb") {
    val conjugatedVerb: ConjugatedVerb = ConjugatedVerb("mangea", "manger", "ind", "psim", "3", Singular)
    val counter: Int = testWord(conjugatedVerb)
    assert(counter > 0)
  }
  test("Noun - split repeated letters") {
    val counter = testWord(Noun("lloo", "lloo", Masculine, Singular))
    assert(counter > 0)
  }
  test("Noun - changer gender or number") {
    val counter = testWord(Noun("maisons", "maison", Feminine, Plural))
    assert(counter > 0)
  }

  private def testWord(word: Word): Int = {
    var counter: Int = 0

    for (_ <- 0 until 100) {
      word.currentWriting = word.correctWriting
      word.counterfeit()
      if (word.isCounterfeited) counter += 1
    }
    println(word.correctWriting + " : " + counter + "% counterfeited")
    counter
  }
}